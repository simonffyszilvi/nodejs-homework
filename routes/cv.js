var express = require('express');
var router = express.Router();


/* POST show CV. */
router.post('/', 
	function(req, res, next) {
		req.checkBody('first_name', 'First name is empty').notEmpty();
		req.checkBody('first_name', 'First name should be at least 3 characters').isLength({ min: 3 });
		req.checkBody('last_name', 'Last name is empty').notEmpty();
		req.checkBody('last_name', 'Last name should be at least 3 characters').isLength({ min: 3 });
		req.checkBody('date', 'Birthdate is empty').notEmpty();

		console.log("lastname: " + req.body.last_name);

  		var errors = req.validationErrors();
	   	if(errors){
	      	req.session.errors = errors;
	      	req.session.success = false;
	      	res.render('index', { title: 'Test App', success: req.session.success, errors: req.session.errors });
   			req.session.errors = null;
	   }
	   else{
	      req.session.success = true;
	      res.render('cv', { title: 'CV page', body: req.body });
	   }
});

module.exports = router;
