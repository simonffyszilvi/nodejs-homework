var express = require('express');
var app = express();
var router = express.Router();

/* GET home page. */
router.get('/', 
	function(req, res, next) {
  		res.render('index', { title: 'Test App', success: [], errors: [] });
});

module.exports = router;
