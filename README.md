
## CV App

Simple Node.js Web application to post information about a profile, and view profile's page.

- node version: v11.10.0
- npm version: 6.8.0

### Used frameworks:
1. express as web application framework https://expressjs.com/
2. bootstrap as front-end framework
3. pug as template engine

---

## Run App
```
npm install --save express express-validator
DEBUG=myapp:* npm start
```

**Open website:** http://localhost:3000/
